#include "esp_interrupt.h"

void app_main(){
    // -- Initialize interrupts on pins defined in header
    interrupt_init();
     // -- Establish a count on an infinite while loop to toggle interrupt signal
    int cnt = 0;
    while(1) {
        printf("cnt: %d\n", cnt++);
        vTaskDelay(2500 / portTICK_RATE_MS);
        gpio_set_level(GPIO_OUTPUT_IO_0, cnt % 2);
        gpio_set_level(GPIO_OUTPUT_IO_1, cnt % 2);
    }
}